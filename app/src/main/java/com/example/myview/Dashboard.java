package com.example.myview;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class Dashboard extends AppCompatActivity implements View.OnClickListener {

    EditText editName, editEmail, editPassword, editCity, editMajor, editMotivation;
    Button btnCancel, btnSign_up;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); //styles
        setContentView(R.layout.activity_dashboard);

        editName = findViewById(R.id.eT_username);
        editEmail = findViewById(R.id.eT_Email);
        editPassword = findViewById(R.id.eT_password);
        editCity = findViewById(R.id.eT_city);
        editMajor = findViewById(R.id.eT_major);
        editMotivation = findViewById(R.id.eT_motivation);
        btnCancel = findViewById(R.id.btn_cancel);
        btnSign_up = findViewById(R.id.btn_sign_up);

        btnSign_up.setOnClickListener(this);
        btnCancel.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sign_up:
                if (isEmpty(editName)) {
                    editName.setError("Major is required!");
                    Toast fullName = Toast.makeText(this, "You must enter fullname to register!", Toast.LENGTH_SHORT);
                    fullName.show();
                }
                if (isEmpty(editPassword)) {
                    editPassword.setError("Password is required!");
                    Toast passWord = Toast.makeText(this, "You must enter password!", Toast.LENGTH_SHORT);
                    passWord.show();
                }

                if (isEmpty(editCity)) {
                    editCity.setError("City is required!");
                }

                if (isEmpty(editMajor)) {
                    editMajor.setError("Major is required!");
                }

                if (isEmpty(editMotivation)) {
                    editMotivation.setError("Motivation is required!");
                }

                if (isEmail(editEmail) == false) {
                    editEmail.setError("Enter valid email!");
                } else {
                    Toast sign = Toast.makeText(this, "Sign Up Succesfully!", Toast.LENGTH_SHORT);
                    sign.show();
                }

                break;
            case R.id.btn_cancel:
                editName.setText("");
                editEmail.setText("");
                editPassword.setText("");
                editCity.setText("");
                editMajor.setText("");
                editMotivation.setText("");

                Toast text = Toast.makeText(this, "Reset Succesfully!", Toast.LENGTH_SHORT);
                text.show();
        }

    }

    boolean isEmail(EditText text) {
        CharSequence email = text.getText().toString();
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

    boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }
}
